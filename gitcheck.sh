# Just in case there are no arguments
Arg1="-n" # Default to no running git status
# check if there are arguments
if [ -n "$1" ]; then # If there are arguments, make the first one define whether or not 'git status' is run
	Arg1=$1
fi
Status="$Arg1" # whether or not you want to run 'git status' as well
TempFile=$(mktemp) # make a temporary file
if [ $Status == "-y" ]; then # do we want to run 'git status'?
	find ~/ -name ".git" -execdir sh -c 'echo "\n$PWD\n" &&  git status' "{}" \; > $TempFile # find any git repo and add the status and location of it to output
else
	find ~/ -name ".git" -execdir sh -c 'echo "$PWD"' "{}" \; > $TempFile # find any git repo and add the location of it to output
fi
grep --color -E '^|ahead of.*|/home/.*|Changes not staged for commit' "$TempFile" # highlight keywords of output and open it in viewing program
